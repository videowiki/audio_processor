require('dotenv').config({ path: '.env' });
const fs = require('fs');
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const PORT = process.env.PORT || 4000;

const APP_DIRS = ['./tmp'];
const { processArticleAudio, updateSubslide } = require('./processAudio');
const DB_CONNECTION = process.env.DB_CONNECTION_URL;

mongoose.connect(DB_CONNECTION)

// Listen on rabbitmq 
require('./worker').init();

// Create necessary file dirs 
APP_DIRS.forEach(dir => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
})


app.all('/*', (req, res, next) => {
  // CORS headers - Set custom headers for CORS
  res.header('Access-Control-Allow-Origin', '*'); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS,PATCH');
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token, X-Vw-Anonymous-Id, X-Key, Cache-Control, X-Requested-With');
  if (req.method === 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

app.use(bodyParser())
app.use(morgan('dev'));

app.get('/', (req, res) => {
  return res.status(200).send('Audio Processor api root');
})

app.post('/process_audio', (req, res) => {
  const { articleId, slidePosition, subslidePosition } = req.body;
  console.log('got request for ', articleId, slidePosition, subslidePosition);
  processArticleAudio(articleId, slidePosition, subslidePosition, (err, result) => {
    let response = {
      articleId,
      slidePosition,
      subslidePosition,
      success: false,
    };

    if (err) {
      console.log('error processing audio', err);
      updateSubslide(articleId, slidePosition, subslidePosition, { audioProcessed: false })
        .then(() => { })
        .catch(err => console.log(err));

      return res.status(400).send('Soemthing went wrong');
    } else if (result && result.success) {
      response.success = true;
      updateSubslide(articleId, slidePosition, subslidePosition, { audioProcessed: true })
        .then(() => {
          return res.json({ result: response });
        })
        .catch((err) => {
          console.log(err);
          return res.status(400).send('Something went wrong');
        })
    }
  });
})

app.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
module.exports = app             // expose app