require('dotenv').config({path: '.env'});
const amqp = require('amqplib/callback_api');
const mongoose = require('mongoose');
const noiseCancellationVideoService = require('./dbHandlers/noiseCancellationVideo');

const DB_CONNECTION = `${process.env.DB_CONNECTION_URL}`;
// const DB_CONNECTION = 'mongodb://localhost/videowiki-en'
console.log('connecting to database ');
// mongoose.connect(DB_CONNECTION)

const { processArticleAudio, updateAudioStatus, processNoiseCancellationVideo } = require('./processAudio');

const PROCESS_RECORDED_AUDIO_QUEUE = `PROCESS_RECORDED_AUDIO_QUEUE`;
const PROCESS_RECORDED_AUDIO_FINISHED_QUEUE = `PROCESS_RECORDED_AUDIO_FINISHED_QUEUE`;

const PROCESS_NOISECANCELLATIONVIDEO_AUDIO_QUEUE = `PROCESS_NOISECANCELLATIONVIDEO_AUDIO_QUEUE`;
const PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE = `PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE`;

let channel;

function init() {
    amqp.connect(process.env.RABBITMQ_SERVER, (err, conn) => {
      if(err) {
          console.log('error is', err);
      }

    conn.createChannel((err, ch) => {
      if (err) {
        console.log('Error creating conection ', err);
        return process.exit(1);
      }
      console.log('connection created')
      channel = ch;
      channel.prefetch(1);
      channel.assertQueue(PROCESS_RECORDED_AUDIO_QUEUE, { durable: true });
      channel.assertQueue(PROCESS_RECORDED_AUDIO_FINISHED_QUEUE, { durable: true });
      channel.assertQueue(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_QUEUE, { durable: true });
      channel.assertQueue(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, { durable: true });
      
      // ch.sendToQueue(PROCESS_AUDIO_QUEUE, new Buffer(JSON.stringify({ articleId: "5ccc3dc303e14d25df136f3d", audioPosition: 0 })))
      channel.consume(PROCESS_RECORDED_AUDIO_QUEUE, processAudioCallback, { noAck: false });
      channel.consume(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_QUEUE, processNoiseCancellationVideoCallback, { noAck: false });
    })
  })
}


function processAudioCallback(msg) {
    const { articleId, slidePosition, subslidePosition } = JSON.parse(msg.content.toString());

    processArticleAudio(articleId, slidePosition, subslidePosition, (err, result) => {
      let response = {
        articleId,
        slidePosition,
        subslidePosition,
        success: false,
      };

      if (err) {
        console.log('error processing audio', err);
        updateAudioStatus(articleId, slidePosition, subslidePosition, { status: 'process_failed', processing: false });
      } else if (result && result.success) {
        response.success = true;
      }

      console.log('acked')
      channel.sendToQueue(PROCESS_RECORDED_AUDIO_FINISHED_QUEUE, new Buffer(JSON.stringify(response)));
      channel.ack(msg);
    });
}

function processNoiseCancellationVideoCallback(msg) {
  const { noiseCancellationVideoId } = JSON.parse(msg.content.toString());
  processNoiseCancellationVideo(noiseCancellationVideoId, (err) => {
    channel.ack(msg);

    if (err) {
      console.log('error', err);
      noiseCancellationVideoService.updateById(noiseCancellationVideoId, {status: 'failed' })
      .then(() => {
        channel.sendToQueue(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, new Buffer(JSON.stringify({ noiseCancellationVideoId })));
      })
      .catch(err => {
        channel.sendToQueue(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, new Buffer(JSON.stringify({ noiseCancellationVideoId })));
        console.log(err);
      })
    } else {
      channel.sendToQueue(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, new Buffer(JSON.stringify({ noiseCancellationVideoId })));
    }
  })
}

module.exports = {
  init,
}